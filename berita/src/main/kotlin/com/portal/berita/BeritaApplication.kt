package com.portal.berita

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class BeritaApplication

fun main(args: Array<String>) {
	runApplication<BeritaApplication>(*args)
}
