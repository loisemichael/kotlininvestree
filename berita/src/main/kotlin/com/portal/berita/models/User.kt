package com.portal.berita.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.sql.Timestamp
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "users")
class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Column(unique = true, name = "username")
    var username: String? = null

    @Column(name = "password")
    var password: String? = null
        @JsonIgnore
        get() = field
        set(value) {
            val passwordEncoder = BCryptPasswordEncoder()
            field = passwordEncoder.encode(value)
        }

    @Column(unique = true, name = "email")
    var email: String? = null

    @Column(name = "name")
    var name: String? = null

    @Column(name = "bod")
    var bod: Date? = null

    @Column(name = "phone")
    var phone: String? = null

    @Column(name = "address")
    var address: String? = null

    fun comparePassword(password: String): Boolean {
        val passwordEncoder = BCryptPasswordEncoder()
        return passwordEncoder.matches(password, this.password)
    }

}