package com.portal.berita.models

import org.w3c.dom.Text
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "news")
class News {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @ManyToOne
    @JoinColumn(name = "user_id")
    var user_id: User?=null

    @Column(name="title")
    var title:String?=null

    @Column(name="category")
    var category:String?=null

    @Column(name="content")
    var content:String?=null

    @Column(name="created_at")
    var created_at:Timestamp?= null

    @Column(name="updated_at")
    var updated_at:Timestamp?= null

}