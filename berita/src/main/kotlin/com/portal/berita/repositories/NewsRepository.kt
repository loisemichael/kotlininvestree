package com.portal.berita.repositories

import com.portal.berita.models.News
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import javax.websocket.server.PathParam

interface NewsRepository : JpaRepository<News, Int> {
//    fun findByTitle(title: String) : News?

    @Query("SELECT n FROM News n where n.title LIKE %:title%")
    fun findByTitle(@PathParam("title") title: String) : List<News>?
}