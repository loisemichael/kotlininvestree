package com.portal.berita.repositories

import com.portal.berita.models.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User,Int> {
    fun findByUsername(username: String) : User?
}