package com.portal.berita.dtos

import java.sql.Timestamp
import java.util.Date

class RegisterDTO {
    val username = ""
    val password = ""
    val email = ""
    val name = ""
    val bod:Date ?= null
    val phone = ""
    val address = ""
}