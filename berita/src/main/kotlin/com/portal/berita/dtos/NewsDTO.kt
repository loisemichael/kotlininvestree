package com.portal.berita.dtos

import com.portal.berita.models.User
import java.sql.Timestamp

class NewsDTO {
    var user_id: User?= User()
    val title = ""
    val category = ""
    val content = ""
    val created_at: Timestamp? =null
    val updated_at: Timestamp? =null

}