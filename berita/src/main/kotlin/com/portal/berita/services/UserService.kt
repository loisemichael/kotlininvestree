package com.portal.berita.services

import com.portal.berita.models.User
import com.portal.berita.repositories.UserRepository
import org.springframework.stereotype.Service

@Service
class UserService(
    private val userRepository: UserRepository
){
    fun getUsers():List<User>{
        return userRepository.findAll()
    }

    fun register(user: User) : User {
        return userRepository.save(user)
    }

    fun findByUsername(username: String) : User? {
        return userRepository.findByUsername(username)
    }


    fun getById(id: Int):User {
        return userRepository.getById(id)
    }

}