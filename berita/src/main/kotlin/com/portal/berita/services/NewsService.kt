package com.portal.berita.services

import com.portal.berita.models.News
import com.portal.berita.models.User
import com.portal.berita.repositories.NewsRepository
import com.portal.berita.repositories.UserRepository
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class NewsService(
    private val newsRepository: NewsRepository,
    private val userRepository: UserRepository
) {

    fun findByTitle(title: String) : List<News>?{
        return newsRepository.findByTitle(title)
    }

    fun getAllNews(): List<News> {
        return newsRepository.findAll(Sort.by(Sort.Direction.DESC,"id"))
    }

//    fun getById(id: Int): User {
//        return userRepository.getById(id)
//    }

    fun getNewsById(id: Int): News {
        return newsRepository.getById(id)
    }

    fun addNews(news: News): News {
        return newsRepository.save(news)
    }

    fun deleteNews(id: Int): String {
        val myNews = newsRepository.getById(id)
        newsRepository.deleteById(id)
        return "Your news about ${myNews.title} has deleted"
    }

    fun updateNews(id: Int, news: News): News {
        val myNews = newsRepository.findById(id).get()
        myNews.title = news.title
        myNews.category = news.category
        myNews.content = news.content
        myNews.updated_at = news.updated_at

        return newsRepository.save(myNews)

    }

}