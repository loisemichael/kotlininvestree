package com.portal.berita.controllers

import com.portal.berita.dtos.LoginDTO
import com.portal.berita.dtos.Message
import com.portal.berita.dtos.RegisterDTO
import com.portal.berita.models.User
import com.portal.berita.services.UserService
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping(value = ["/api"], produces = [MediaType.APPLICATION_JSON_VALUE])
class AuthController @Autowired constructor(
    private val userService: UserService
) {
    var loggedIn = User()

    @GetMapping("/allUser")
    fun getAllUsers(): ResponseEntity<List<User>> {
        return ResponseEntity(userService.getUsers(), HttpStatus.OK)
    }

    @PostMapping("/register")
    fun register(
        @RequestBody body: RegisterDTO
    ): ResponseEntity<User> {
        val user = User()
        user.username = body.username
        user.password = body.password
        user.email = body.email
        user.name = body.name
        user.bod = body.bod
        user.phone = body.phone
        user.address = body.address
        return ResponseEntity(userService.register(user), HttpStatus.OK)
    }

    @PostMapping("login")
    fun login(
        @RequestBody body: LoginDTO,
        response: HttpServletResponse
    ): ResponseEntity<Any> {

        val user = userService.findByUsername(body.username)
        if (user == null) {
            return ResponseEntity.badRequest().body(Message("Akun tidak ditemukan!"))
        } else if (!user.comparePassword(body.password)) {
            return ResponseEntity.badRequest().body(Message("Username atau password salah!"))
        }

        val issuer = user.id.toString()

        val jwt = Jwts.builder()
            .setIssuer(issuer)
            .setExpiration(Date(System.currentTimeMillis() + 1 * 60 * 1000)) // 1 hours
            .signWith(SignatureAlgorithm.HS256, "secret").compact()

        val cookie = Cookie("jwt", jwt)
        cookie.isHttpOnly = true



        response.addCookie(cookie)

        return ResponseEntity.ok(Message("Sukses Validated"))
    }

    @GetMapping("user")
    fun user(
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {
        try {
            if (jwt == null) {
                return ResponseEntity.status(601).body(Message("Silahkan Login Terlebih Dahulu"))
            }

            val body = Jwts.parser().setSigningKey("secret").parseClaimsJws(jwt).body

            loggedIn = getLoggedIn(body.issuer)

            return ResponseEntity.ok(userService.getById(body.issuer.toInt()))
        } catch (e: Exception) {
            return ResponseEntity.status(601).body(Message("Silahkan Login Terlebih Dahulu"))
        }
    }

    @PostMapping("logout")
    fun logout(
        response: HttpServletResponse
    ): ResponseEntity<Any> {
        var cookie = Cookie("jwt", "")
        cookie.maxAge = 0

        response.addCookie(cookie)

        return ResponseEntity.ok(Message("Berhasil Logout"))
    }

    fun getLoggedIn(myIssuer: String): User {
        return userService.getById(myIssuer.toInt())
    }


}