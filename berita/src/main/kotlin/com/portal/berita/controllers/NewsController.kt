package com.portal.berita.controllers

import com.portal.berita.dtos.Message
import com.portal.berita.dtos.NewsDTO
import com.portal.berita.dtos.SearchDTO
import com.portal.berita.models.News
import com.portal.berita.services.NewsService
import io.jsonwebtoken.Jwts
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = ["/news"], produces = [MediaType.APPLICATION_JSON_VALUE])
class NewsController @Autowired constructor(
    private val newsService: NewsService,
    private val authController: AuthController
) {
    @GetMapping("/allNews")
    fun getAllNews(): ResponseEntity<List<News>> {
        return ResponseEntity.ok(newsService.getAllNews())
    }

    @PostMapping("/addNews")
    fun addNews(
        @RequestBody bodyNews: NewsDTO,
        @CookieValue("jwt") jwt: String?
    ): ResponseEntity<Any> {
        try {
            val myBody = authController.loggedIn

            val myNews = News()
            bodyNews.user_id = myBody
            myNews.user_id = bodyNews.user_id
            myNews.title = bodyNews.title
            myNews.category = bodyNews.category
            myNews.content = bodyNews.content
            myNews.created_at = bodyNews.created_at
            myNews.updated_at = bodyNews.updated_at

            return ResponseEntity.ok(newsService.addNews(myNews))
        } catch (e: Exception) {
            return ResponseEntity.status(601).body(Message("Silahkan Login Terlebih Dahulu"))
        }
    }

    @DeleteMapping("deleteNews/{id}")
    fun deleteMyNews(
        @PathVariable("id") id: Int
    ): ResponseEntity<Any> {
        val checkUser = authController.loggedIn
        val news: News = newsService.getNewsById(id)

        if (checkUser.id != news.user_id?.id) {
            return ResponseEntity.status(601).body(Message("I'm sorry, you don't have access"))
        }

        return ResponseEntity.ok(newsService.deleteNews(id))
    }

    @PutMapping("updatedNews/{id}")
    fun updateMyNews(
        @PathVariable("id") id: Int,
        @RequestBody newsCheck: NewsDTO
    ): ResponseEntity<Any> {
        val checkUser = authController.loggedIn
        val news: News = newsService.getNewsById(id)

        news.title = newsCheck.title
        news.category = newsCheck.category
        news.content = newsCheck.content
        news.updated_at = newsCheck.updated_at

        if (checkUser.id != news.user_id?.id) {
            return ResponseEntity.status(601).body(Message("I'm sorry, you don't have access"))
        }

        return ResponseEntity.ok(newsService.updateNews(id, news))
    }

    @GetMapping("News/Search")
    fun searchByTitle(
        @RequestParam("key") key: String
    ): ResponseEntity<Any> {
        return ResponseEntity.ok(newsService.findByTitle(key))
    }


//    fun user(
//        @CookieValue("jwt")jwt: String?
//    ):ResponseEntity<Any>{
//        try {
//            if (jwt == null) {
//                return ResponseEntity.status(601).body(Message("Silahkan Login Terlebih Dahulu"))
//            }
//
//            val body = Jwts.parser().setSigningKey("secret").parseClaimsJws(jwt).body
//
//
//
//            return ResponseEntity.ok(newsService.getById(body.issuer.toInt()))
//        } catch (e: Exception){
//            return ResponseEntity.status(601).body(Message("Silahkan Login Terlebih Dahulu"))
//        }
//    }
}