class LoginServiceImpl : LoginService{
    override fun login(username: String, password: String): Boolean {
        val result = when{
            username.isEmpty() -> return false
            username.equals("loise.michael@investree.id") -> true
            else -> false
        }

        return result
    }
}
