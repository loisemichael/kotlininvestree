package service

import database.UserDatabase
import database.UserDatabaseImpl
import model.UserData

class CredentialServiceImpl : CredentialService {

    private val database: UserDatabase = UserDatabaseImpl()
    private var loggedInUser: UserData? = null
    private val myUser = UserDatabaseImpl();
    private val sizeUser = myUser.sizeUser


    override fun doLogin(username: String, password: String): UserData? {
        if (username.isEmpty() || password.isEmpty()) {
            println("username dan password harus diisi")
            return null
        }

        val userData: UserData? = database.findUser(username)
        if (userData == null) {
            println("user tidak ditemukan")
            return null
        }
        if (!userData.password.equals(password, false)) {
            println("password tidak sama")
            return null
        }

        println("user berhasil login")
        loggedInUser = userData
        return userData

    }

    override fun getLoggedInUser(): UserData? = loggedInUser

    override fun doRegister(name: String, username: String, password: String, address: String): UserData? {
        if (name.isEmpty() || username.isEmpty() || password.isEmpty() || address.isEmpty()) {
            println("Data harus di isi")
        }

        val myData = UserData(sizeUser+1,name,username,password,address)
        database.addUser(myData)
        return myData
    }
}