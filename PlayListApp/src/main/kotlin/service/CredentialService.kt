package service

import model.UserData

interface CredentialService {
    fun doLogin(username: String, password: String): UserData?

    fun getLoggedInUser() : UserData?

    fun doRegister(name: String,username: String, password: String,address: String): UserData?
}