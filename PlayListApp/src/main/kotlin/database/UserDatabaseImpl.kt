package database

import model.UserData

class UserDatabaseImpl : UserDatabase {
    private val userList = mutableListOf(
        UserData(1, "Loise Lumban Raja", "loise1807", "loise123"),
        UserData(2, "Daud Manurung", "daudmanru22", "daud123", "Uluan"),
        UserData(3, "Jonathan B Hutajulu", "jonathanbh", "jonathan123", "Laguboti"),
    )

    public var sizeUser = userList.size

    override fun findAllUser(): List<UserData> = userList

    override fun findUser(username: String): UserData? {
//        for (loop in 0..userList.size) {
//            val selectedUserCheck = userList[loop]
//            if (selectedUserCheck.username.equals(username, true)) return selectedUserCheck
//        }
//        return null

//        userList.forEach {
//            if (it.username.equals(username,true)) return it
//        }
//        return null

        return userList.find{it.username.equals(username,true)}
    }

    override fun addUser(data: UserData) {
        userList.add(data)
    }
}