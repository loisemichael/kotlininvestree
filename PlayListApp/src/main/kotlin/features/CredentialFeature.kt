package features
import model.UserData
import service.CredentialService
import service.CredentialServiceImpl
import java.util.Scanner

class CredentialFeature(val scanner: Scanner) {
    private val credentialService: CredentialService = CredentialServiceImpl()
    fun login(){
        print("Username: ")
        val inputUsername = scanner.nextLine()
        print("Password: ")
        val inputPassword = scanner.nextLine()

        credentialService.doLogin(inputUsername,inputPassword)?.let {
            printUserData(it)
        }
    }

    fun getProfile() {
        credentialService.getLoggedInUser()?.let {
            printUserData(it)
        }?: print("anda belum login")

    }

    private fun printUserData(userData: UserData) {
        println("Username : ${userData.username}")
        println("Nama : ${userData.name}")
        println("Address : ${userData.address}")
    }

    fun getRegister(){
        println("")
        println("==========================")
        println("   Formulir Pendaftaran   ")
        println("==========================")
        println("Silahkan mengisi data Diri Anda")
        print("Name: ")
        val regName = scanner.nextLine()
        print("Username: ")
        val regUsername = scanner.nextLine()
        print("Password: ")
        val regPassword = scanner.nextLine()
        print("Address: ")
        val regAddress = scanner.nextLine()

        credentialService.doRegister(regName,regUsername,regPassword,regAddress)
    }
}