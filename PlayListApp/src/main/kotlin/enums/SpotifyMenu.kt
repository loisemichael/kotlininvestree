package enums

enum class SpotifyMenu(val id:String, val desc:String) {
    LOGIN("1","Silahkan Login"),
    REGISTER("2","Register Menu"),
    PROFILE("3","Profile Menu")
}
