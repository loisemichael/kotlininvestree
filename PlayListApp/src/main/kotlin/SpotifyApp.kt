import enums.SpotifyMenu
import features.CredentialFeature
import java.util.Scanner

private val scanner = Scanner(System.`in`)

private lateinit var credentialFeature: CredentialFeature

fun main(){
    setup()
    welcome()
}

fun welcome() {

    println("")
    println("======================")
    println("-------Welcome--------")
    println("======================")
    println("1. Login")
    println("2. Register")
    println("3. Profile")
    print("Pilih Menu: ")

    when (scanner.nextLine()) {
        SpotifyMenu.LOGIN.id -> credentialFeature.login()
        SpotifyMenu.REGISTER.id -> credentialFeature.getRegister()
        SpotifyMenu.PROFILE.id -> credentialFeature.getProfile()
        else -> println("Maaf menu yang dipilih tidak ada")
    }
    welcome()
}





fun setup() {
    credentialFeature = CredentialFeature(scanner)
}
