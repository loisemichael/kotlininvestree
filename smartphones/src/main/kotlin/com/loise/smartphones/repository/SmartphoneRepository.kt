package com.loise.smartphones.repository

import com.loise.smartphones.entity.Smartphone
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SmartphoneRepository: JpaRepository<Smartphone,Int>