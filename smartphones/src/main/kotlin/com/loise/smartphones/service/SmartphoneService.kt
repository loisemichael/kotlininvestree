package com.loise.smartphones.service

import com.loise.smartphones.entity.Smartphone
import com.loise.smartphones.repository.SmartphoneRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.Optional


@Service
class SmartphoneService @Autowired constructor(
    private val smartphoneRepository: SmartphoneRepository
){
    fun getSmartphones():List<Smartphone>{
        return smartphoneRepository.findAll()
    }

    fun getDetailSmartphone(id:Int): Optional<Smartphone>{
        return smartphoneRepository.findById(id)
    }

    fun addSmartphone(smartphone: Smartphone):Smartphone{
        return smartphoneRepository.save(smartphone)
    }

    fun updateSmartphone(id:Int,smartphone: Smartphone):Smartphone{
        val updateSmartphone = smartphoneRepository.findById(id).get()
        updateSmartphone.no_rangkai = smartphone.no_rangkai
        updateSmartphone.merek = smartphone.merek
        updateSmartphone.tipe = smartphone.tipe
        updateSmartphone.warna = smartphone.warna
        updateSmartphone.penyimpanan = smartphone.penyimpanan
        updateSmartphone.ram = smartphone.ram
        return smartphoneRepository.save(updateSmartphone)
    }

    fun deleteBook(id:Int):String{
        val checkSmartphone = smartphoneRepository.findById(id)
        if(checkSmartphone == null){
            throw Exception()
        }else{
            smartphoneRepository.deleteById(id)
            return "Smartphone dengan id: $id sudah dihapus dari database!"
        }
    }
}