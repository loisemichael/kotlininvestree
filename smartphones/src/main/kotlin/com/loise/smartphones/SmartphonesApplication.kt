package com.loise.smartphones

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SmartphonesApplication

fun main(args: Array<String>) {
	runApplication<SmartphonesApplication>(*args)
}
