package com.loise.smartphones.entity

import javax.persistence.*

@Entity
@Table(name = "smartphones")
data class Smartphone(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,

    @Column(name = "no_rangkai")
    var no_rangkai: String? = null,

    @Column(name = "merek")
    var merek: String? = null,

    @Column(name = "tipe")
    var tipe: String? = null,

    @Column(name = "warna")
    var warna: String? = null,

    @Column(name = "penyimpanan")
    var penyimpanan: Int? = null,

    @Column(name = "ram")
    var ram: Int? = null

)
