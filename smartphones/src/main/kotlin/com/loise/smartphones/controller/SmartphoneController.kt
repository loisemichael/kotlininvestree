package com.loise.smartphones.controller

import com.loise.smartphones.entity.Smartphone
import com.loise.smartphones.service.SmartphoneService
import org.springframework.stereotype.Controller
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import java.util.*

@Controller
@RequestMapping(value = ["/smartphone"], produces = [MediaType.APPLICATION_JSON_VALUE])
class SmartphoneController @Autowired constructor(
    private val smartphoneService:SmartphoneService
){
    @GetMapping
    fun getAllSmartphones():ResponseEntity<List<Smartphone>>{
        return ResponseEntity(smartphoneService.getSmartphones(),HttpStatus.OK)
    }

    @GetMapping("{id}")
    fun getDetailSmartphones(
        @PathVariable("id") id: Int
    ):ResponseEntity<Optional<Smartphone>>{
        return ResponseEntity(smartphoneService.getDetailSmartphone(id),HttpStatus.OK)
    }

    @PostMapping
    fun addNewSmartphone(
        @RequestBody smartphone: Smartphone
    ):ResponseEntity<Smartphone>{
        return ResponseEntity(smartphoneService.addSmartphone(smartphone),HttpStatus.OK)
    }

    @PutMapping("{id}")
    fun updateSmartphone(
        @PathVariable("id") id:Int,
        @RequestBody smartphone: Smartphone
    ):ResponseEntity<Smartphone>{
        return ResponseEntity(smartphoneService.updateSmartphone(id,smartphone),HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    fun deleteSmartphone(
        @PathVariable("id") id:Int
    ):ResponseEntity<String>{
        return ResponseEntity(smartphoneService.deleteBook(id),HttpStatus.OK)
    }
}