package Tutorial.ScopeFunction

fun main() {

    val user = User("Loise")
    val user2 = User(null)

    user.run{
        name = "rifky"
    }

    user.let{
        it.name = "Loise"
    }

    println(user)
}

