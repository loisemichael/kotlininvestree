package Tutorial.ScopeFunction

fun main() {
    val user = User("Loise")
    val user2 = User(null)
    validateUser(user)
    validateUser(user2)
}

fun validateUser(user: User){
//    if (user.name == null){
//        print("You are anonymous!")
//    }else{
//        print("Your name is ${user.name}")
//    }

    user.name.let{
    println("Your name is $it")
    } ?: println("Who are U?")

//    user.name.let{name->
//    println("Your name is $name")
//    } ?: println("Who are U?")
}

data class User(
    var name: String? = null,
    var age: Int? = null,
    var sex: String? = null
)