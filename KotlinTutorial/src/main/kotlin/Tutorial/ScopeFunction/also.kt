package Tutorial.ScopeFunction

fun main() {
    val user = User().apply {
        name = "Loise"
        age = 20
        sex = "Male"
    }
    .also { println("Sorting the list") }
}