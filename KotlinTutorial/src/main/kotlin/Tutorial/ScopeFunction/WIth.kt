package Tutorial.ScopeFunction

fun main() {
    val user1 = User("Loise")
    user1.run {
        name= "Loise Michael"
     }

    println(user1)

    with(user1){
        name = "Mickhael Loise"
    }
    println(user1)
}