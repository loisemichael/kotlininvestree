package Tutorial.Basic

class Mahasiswa {
    var name: String = "Nama Mahasiswa";
}

fun main() {
    val mahasiswa = Mahasiswa()
    mahasiswa.name = "Savino"
    println(mahasiswa.name)
}