fun main() {
//    greet(ln="Budi")
//    greet(fn="Budi",ln="Setiawan")
    greet()
}

fun greet(): Unit{
    println("Loise")
}

// Named Arguments
//fun greet(fn: String="", ln: String) {
//    println("Hello $fn $ln")
//}

// Default Arguments
//fun greet(fn: String, ln: String="") {
//    println("Hello $fn $ln")
//}

// Function Arguments
//fun greet(fn: String, ln: String) {
//    println("Hello $fn $ln")
//}

//fun greet(name: String) {
//    println("Hello $name")
//}




