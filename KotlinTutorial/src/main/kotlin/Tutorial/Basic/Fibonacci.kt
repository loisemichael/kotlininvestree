fun main() {
    print(fibonacci(10))
}

fun fibonacci (num:Int): Int {
    if(num<2){
        return num
    }
    return fibonacci(num-1) + fibonacci(num -2)

}