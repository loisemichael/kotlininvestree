fun main() {
    val oneToHundred = 1 .. 100 step 10
    val hundredToOne = 100 downTo 1 step 23

    println(hundredToOne.step)
}