fun main() {
    for (i in 1..50){
        if (i%5 == 0) {
            println("$i <- Kelipatan 5")
            continue
        }
        println(i)
    }
}