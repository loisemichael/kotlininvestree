fun main() {
    var x : Int = 2
    when(x){
        1 -> print("x == 1")
        2 -> print("x == 2")
        else -> {
            print("x neither 1 or 2")
        }
    }
}