package Tutorial.OOP.abstractclass

abstract class Kendaraan{
    abstract var warna:String
    fun startEngine(){
        println("Brrrrrmmm....")
    }

    abstract fun bahanBakar()
}

class Mobil: Kendaraan(){
    override var warna: String = "Kuning"
    override fun bahanBakar() {
        println("Pertamax")
    }
}
class Motor: Kendaraan(){
    override var warna: String = "Kuning"
    override fun bahanBakar() {
        println("Pertalite")
    }
}

fun main() {
    val mobil = Mobil()
    mobil.startEngine()
    mobil.bahanBakar()
}
