package Tutorial.OOP.encapsulation

class Lender {
    var email : String = "loise.michael@investree.id"
    get() {
        println("Value lender.email = $field")
        return field
    }
    set(value){
        field=value.uppercase()
    }
}

fun main() {
    val loise = Lender()
    println(loise.hashCode())
}