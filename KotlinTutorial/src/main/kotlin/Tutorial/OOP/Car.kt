package Tutorial.OOP

class Car {
    var brand = ""
    var model = ""
    var year = 0
}

fun main() {
    val c1 = Car()
    c1.brand = "Toyota"
    c1.model = "Innova"
    c1.year = 2012
    val c2 = Car()
    c2.brand = "Mitsubishi"
    c2.model = "Pajero Sport"
    c2.year = 2015

    println("Mobil brand ${c1.brand} dengan model ${c1.model} telah keluar pada tahun ${c1.year}")
    println("Mobil brand ${c2.brand} dengan model ${c2.model} telah keluar pada tahun ${c2.year}")
}