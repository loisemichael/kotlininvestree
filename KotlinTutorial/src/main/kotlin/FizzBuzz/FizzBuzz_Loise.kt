import java.util.*

fun main() {
    val myScan = Scanner(System.`in`)
    print("Enter a number: ")

    var x:Int = myScan.nextInt()

    fizzBuzz(x)
}

fun fizzBuzz(x:Int){
    for(i in 1..x){
        if(i%3 == 0 && i%5 == 0){
            println("FizzBuzz")
            continue
        }else if(i%3 == 0){
            println("Fizz")
            continue
        }else if(i%5 == 0){
            println("Buzz")
        }else println(i)
    }
}