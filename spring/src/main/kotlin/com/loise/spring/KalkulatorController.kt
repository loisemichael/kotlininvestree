package com.loise.spring

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/kalkulator")
class KalkulatorController (private val kalkulator: Kalkulator)
{

    /**
     *URL: /kalkulator/tambah?angka1=10&angka2=5 *
     */

    @GetMapping("/tambah")
    fun actionTambah(
        @RequestParam("angka1") angka1: Int,
        @RequestParam("angka2") angka2: Int
    ): ResponseEntity<String>{
        val result = kalkulator.tambah(angka1,angka2)
        val response = "$angka1 + $angka2 = $result"
        return ResponseEntity(response, HttpStatus.OK)
    }
}