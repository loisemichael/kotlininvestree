class Romawi {
}

fun romanToInteger(roman: String) : Int{
    val myRoman : HashMap<Char,Int> = HashMap<Char,Int>()

    myRoman.put('I',1)
    myRoman.put('V',5)
    myRoman.put('X',10)
    myRoman.put('L',50)
    myRoman.put('C',100)
    myRoman.put('D',500)
    myRoman.put('M',1000)

    var myNumber: Int = 0
    var myChar: Char?


    for (i in 0 .. (myRoman.size)-1){
        myChar = roman[i]

        if(i>0 && myRoman[myChar]!! > myRoman[roman[i-1]]!!){
//            print("$myNumber->")
            myNumber += (myRoman[myChar]!! - 2*myRoman[roman[i-1]]!!)
//            var doubleMyRoman = 2* myRoman[roman[i-1]]!!
//            println("${myRoman[myChar]}($myChar) - $doubleMyRoman (${roman[i - 1]}) - ${myRoman[roman[i-1]]} = $myNumber")
        }else{
            myNumber += myRoman[myChar]!!
        }
//    println("$myNumber when i = $i")
    }

    return myNumber

}

fun main() {
    val exampleRoman: String = "MCMXCIV";
    var exampleResult: Int = romanToInteger(exampleRoman);

    println(exampleResult)
}